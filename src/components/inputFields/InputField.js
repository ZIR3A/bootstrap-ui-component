import React from "react";
import PropTypes from "prop-types";

const InputField = ({
  size = "md",
  width = "auto",
  label,
  name,
  onChange,
  readOnly,
  type,
  required,
  maxLength,
  minLength,
  value,
  validator,
  helpertext,
  className,
  focus = false,
  placeHolder,
  ...others
}) => {
  let length = "auto";

  if (width === "sm") length = 25;
  if (width === "md") length = 50;
  if (width === "lg") length = 100;

  return (
    <>
      {/* <div className={`form-group flex-fill`}> */}
      <div className={`form-group flex-fill w-${length}`}>
        {label ? <label className="form-label opacity-75">{label}</label> : ""}
        {label && required && (
          <span className="text-danger h6 ml-1">
            <b>*</b>
          </span>
        )}
        <input
          className={
            required
              ? !validator
                ? `is-invalid form-control ${
                    size ? "form-control-" + size : "form-control-md"
                  } ${className}  w-${length}`
                : `is-valid form-control ${
                    size ? "form-control-" + size : "form-control-md"
                  } ${className} w-${length}`
              : `form-control ${
                  size ? "form-control-" + size : "form-control-md"
                } ${className} w-${length}`
          }
          placeholder={placeHolder}
          autoFocus={focus}
          name={name}
          type={type}
          id={name}
          value={value}
          required={required}
          {...(maxLength
            ? {
                max: maxLength,
              }
            : {})}
          {...(minLength
            ? {
                min: minLength,
              }
            : {})}
          readOnly={readOnly}
          onChange={onChange}
          {...others}
        />

        <div className="invalid-feedback">
          {helpertext || "Please Enter Valid Info"}
        </div>
      </div>
    </>
  );
};

InputField.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  readOnly: PropTypes.bool,
  type: PropTypes.string,
  helpertext: PropTypes.string,
  required: PropTypes.bool,
  focus: PropTypes.bool,
  maxLength: PropTypes.number,
  minLength: PropTypes.number,
  value: PropTypes.string,
  width: PropTypes.oneOf(["sm", "md", "lg"]),
  onChange: PropTypes.func,
};

export default InputField;

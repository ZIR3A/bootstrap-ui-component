import { InputFields } from "../components/inputFields/InputField";

export default {
  title: "Bootstrap - UI Components/InputField",

  component: InputFields,
  argTypes: { onChange: { action: "onChange" } },
};

const Template = (args) => <InputFields {...args} />;

export const TextField = Template.bind({});

TextField.args = {
  type: "text",
  name: "Input Text",
  label: "Input Text",
  helperText: "Custome Helper Text",
};

export const Email = Template.bind({});
Email.args = {
  focus: true,
  type: "email",
  name: "Input  Email",
  label: "Input  Email",
};

export const Number = Template.bind({});
Number.args = {
  type: "number",
  name: "Input Number",
  label: "Input Number",
};
export const File = Template.bind({});
File.args = {
  type: "file",
  name: "Input File",
  label: "Input File",
};
export const Tel = Template.bind({});
Tel.args = {
  type: "tel",
  name: "Input Tel",
  label: "Input Tel",
};
